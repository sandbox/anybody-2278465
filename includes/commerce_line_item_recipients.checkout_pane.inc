<?php

/**
 * Implements hook_commerce_pane_checkout_form().
 * We embed the Views Megarow table of order line items.
 */
function commerce_line_item_recipients_add_recipients_pane_checkout_form($form, $form_state, $checkout_pane, $order) {
  // Load the completion message.
  $message = variable_get('commerce_line_item_recipients_checkout_message', array('value' => '', 'format' => 'filtered_html'));
  if (module_exists('i18n')) {
    // Perform translation.
    $message['value'] = commerce_i18n_string('commerce:commerce_line_item_recipients:message', $message['value'], array('sanitize' => FALSE));
  }
  if (module_exists('token')) {
    // Perform token replacement.
    $message['value'] = token_replace($message['value'], array('commerce-order' => $order), array('clear' => TRUE));
  }
  // Apply the proper text format.
  $message['value'] = check_markup($message['value'], $message['format']);
  $form['message'] = array(
    '#markup' => '<div class="checkout-message">' . $message['value'] . '</div>',
  );

  $form['line_item_recipients'] = array(
    '#markup' => commerce_embed_view('line_item_recipients', 'block', array($order->order_id)),
  );

  return $form;
}

function commerce_line_item_recipients_add_recipients_pane_settings_form($checkout_pane) {
  $form = array();

  $message = variable_get('commerce_line_item_recipients_checkout_message', array('value' => '', 'format' => 'filtered_html'));
  $form['container'] = array(
    '#type' => 'container',
    '#access' => filter_access(filter_format_load($message['format'])),
  );
  $form['container']['commerce_line_item_recipients_checkout_message'] = array(
    '#type' => 'text_format',
    '#title' => t('Checkout message'),
    '#default_value' => $message['value'],
    '#format' => $message['format'],
  );

  $var_info = array(
    'site' => array(
      'type' => 'site',
      'label' => t('Site information'),
      'description' => t('Site-wide settings and other global information.'),
    ),
    'commerce_order' => array(
      'label' => t('Order'),
      'type' => 'commerce_order',
    ),
  );

  $form['container']['commerce_line_item_recipients_checkout_message_help'] = RulesTokenEvaluator::help($var_info);

  return $form;
}
