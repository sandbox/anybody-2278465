<?php

class commerce_line_item_recipients_handler_recipient_address extends views_handler_field_entity {

  function render($values) {
    $element = array();

    $line_item = commerce_line_item_load($this->get_value($values, 'line_item_id'));
    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

    $recipient_address_ref = $line_item_wrapper->field_ref_recipient_address->value();
    if (!empty($recipient_address_ref) && is_object($recipient_address_ref) && !empty($recipient_address_ref->commerce_customer_address)) {
      $shipping = entity_metadata_wrapper('commerce_customer_profile', $recipient_address_ref);
      $recipient_address = $shipping->commerce_customer_address->value();
      // create an edit recipient link
      $element['edit_recipient'] = array(
        '#markup' => l(t('Edit Recipient'), "commerce_line_item_recipients/{$line_item_wrapper->line_item_id->value()}/edit", array(
          'attributes' => array(
            'class' => array('views-megarow-open btn commerce-recipient-edit'),
          ),
        )),
        '#weight' => 100,
        '#prefix' => '<div class="commerce-recipient-operations">',
        '#suffix' => '</div>',
      );
    }
    else {
      // if user has yet to add a recipient address use the billing address on the order
      $order = entity_metadata_wrapper('commerce_order', $line_item_wrapper->order->value());
      $billing = entity_metadata_wrapper('commerce_customer_profile', $order->commerce_customer_billing->value());
      $recipient_address = $billing->commerce_customer_address->value();


      // create an add recipient link
      $element['add_recipient'] = array(
        '#markup' => l(t('Edit Recipient'), "commerce_line_item_recipients/{$line_item_wrapper->line_item_id->value()}/add", array(
          'attributes' => array(
            'class' => array('views-megarow-open btn commerce-recipient-add'),
          )
        )),
        '#weight' => 100,
        '#prefix' => '<div class="commerce-recipient-operations">',
        '#suffix' => '</div>',
      );
    }

    // generate a render array for the address
    $h = array('address' => 'address');
    $element += addressfield_generate($recipient_address, $h);

    return $element;
  }

}
