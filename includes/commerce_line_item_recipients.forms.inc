<?php

function commerce_line_item_recipients_add($op = 'edit', $line_item_id) {
  $line_item = commerce_line_item_load($line_item_id);
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  if ($op == 'edit') {
    $profile = $line_item_wrapper->field_ref_recipient_address->value();
  }
  elseif ($op == 'add') {
    $order = entity_metadata_wrapper('commerce_order', $line_item_wrapper->order->value());
    $profile = commerce_customer_profile_new('shipping', $order->uid->value());
  }

  $recipient_form = drupal_get_form('commerce_line_item_recipients_form', $profile, $line_item_id, $op);
  $refresh = !empty($recipient_form['#refresh_line_item_row']);
  $output = drupal_render($recipient_form);
  $return = views_megarow_display(t('Recipient', array(), array('context' => 'comerce_recipient:views_megarow_title')), $output, $line_item_id);

  // If a recipient was just saved, close the form and refresh the line item row in the view
  if ($refresh) {
    // We need to get the order the line item belongs to
    if (empty($line_item)) {
      $line_item = commerce_line_item_load($line_item_id);
    }
    $return['#commands'] = array();
    $return['#commands'][] = views_megarow_command_refresh_parent($line_item_id, 'block', array($line_item->order_id));
    $return['#commands'][] = views_megarow_command_dismiss($line_item_id);
  }

  return $return;
}

/**
 * Generates the entity add/edit form
 *
 */
function commerce_line_item_recipients_form($form, &$form_state) {
  $form = array();
  $form['#prefix'] = "<div id='commerce-recipient-form'>";
  $form['#suffix'] = "</div>";
  $profile = $form_state['build_info']['args'][0];
  $line_item_id = $form_state['build_info']['args'][1];
  $op = $form_state['build_info']['args'][2];

  // Add the field related form elements.
  $form_state['customer_profile'] = $profile;
  field_attach_form('commerce_customer_profile', $profile, $form, $form_state);

  $line_item = commerce_line_item_load($line_item_id);
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
  $line_item_plz = $line_item_wrapper->field_buy_abstellort_plz->value();

  $form['commerce_customer_address'][LANGUAGE_NONE][0]['locality_block']['postal_code']['#value'] = $line_item_plz;
  $form['commerce_customer_address'][LANGUAGE_NONE][0]['locality_block']['postal_code']['#disabled'] = TRUE;

  // attach submit button
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => ($op === 'edit') ? t('Save Recipient') : t('Add Recipient'),
    '#weight' => 50,
  );

  // If we just saved something, we need to let the flag bubble up to the page callback
  if (!empty($form_state['recipient_saved'])) {
    $form['#refresh_line_item_row'] = TRUE;
  }

  return $form;
}

/**
 * Form API Submit callback for the Recipient add/edit form
 */
function commerce_line_item_recipients_form_submit($form, &$form_state) {
  $profile = &$form_state['customer_profile'];
  $line_item_id = $form_state['build_info']['args'][1];

  $line_item = commerce_line_item_load($line_item_id);
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
  $order = $line_item_wrapper->order->value();

  // Ensure the profile is active.
  $profile->status = TRUE;

  // Set the profile's uid if it's being created at this time.
  if (empty($profile->profile_id)) {
    $profile->uid = $order->uid;
  }

  // Add the entity context of the current line item.
  $profile->entity_context = array(
    'entity_type' => 'commerce_line_item',
    'entity_id' => $line_item_id,
  );

  // Notify field widgets.
  field_attach_submit('commerce_customer_profile', $profile, $form, $form_state);
  // Save the profile.
  commerce_customer_profile_save($profile);

  $line_item_wrapper->field_ref_recipient_address->set($profile);
  $line_item_wrapper->save();
  $form_state['rebuild'] = TRUE;
  $form_state['recipient_saved'] = TRUE;
}

function commerce_line_item_recipients_load_by_name() {
  return false;
}
